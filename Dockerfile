FROM ubuntu:jammy as build

ARG JGR_VERSION=0.59.1
ARG GFX_VERSION=0.5.5
ENV DEBIAN_FRONTEND=noninteractive
ENV VCPKG_FORCE_SYSTEM_BINARIES=1


RUN rm /var/lib/dpkg/info/libc-bin.*
RUN apt-get clean
RUN apt-get update
RUN apt-get install libc-bin


RUN apt-get update && apt-get install -y --no-install-recommends  \
  ca-certificates \
  patch \
  git \
  unzip zip tar \
  gcc \
  curl \
  g++ \
  make \
  cmake \
  ninja-build \
  libgomp1 \
  libglib2.0 \
  zlib1g-dev \
  liblzma-dev \
  liblzo2-dev \
  xz-utils \ 
  liballegro4-dev \
  libcurl4-openssl-dev \
  libfontconfig-dev \
  libharfbuzz-dev \
  libicu-dev \
  liblzma-dev \
  libzstd-dev \
  liblzo2-dev \
  libsdl2-dev \
  grfcodec \
  autoconf autoconf-archive

RUN mkdir /src /build /app

WORKDIR /build

RUN git clone --depth 1 --branch jgrpp-${JGR_VERSION} https://github.com/JGRennison/OpenTTD-patches.git /src

# Install vcpkg
# RUN git clone https://github.com/microsoft/vcpkg /temp/vcpkg \
#   && /temp/vcpkg/bootstrap-vcpkg.sh -disableMetrics \
#   && /temp/vcpkg/vcpkg update 

# Build
RUN cmake /src \
    -DCMAKE_SOURCE_DIR=/src \
    -DCMAKE_CXX_FLAGS_INIT="-DRANDOM_DEBUG" \
    -DCMAKE_DISABLE_PRECOMPILE_HEADERS=ON \
    -DCMAKE_BUILD_TYPE=release \
    -DOPTION_INSTALL_FHS=OFF \
    -DGLOBAL_DIR=/app \
    -DCMAKE_INSTALL_PREFIX=/app
RUN make CMAKE_BUILD_TYPE=release -j $(nproc)
RUN cpack \
  && make install

FROM ubuntu:jammy

ARG GFX_VERSION=0.5.5
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y  --no-install-recommends  \
  curl \
  git \
  unzip \
  libgomp1 \
  libglib2.0 \
  liblzo2-2 \
  xz-utils \
  libpng16-16 \
  libsdl2-2.0-0 \
  libcurl4 liballegro4.4 libfreetype6 libfontconfig1 \
  libharfbuzz0b

COPY --from=build /app /app

RUN mkdir /extract
WORKDIR /extract

RUN curl -o opengfx-${GFX_VERSION}.zip http://bundles.openttdcoop.org/opengfx/releases/${GFX_VERSION}/opengfx-${GFX_VERSION}.zip \
    && unzip opengfx-${GFX_VERSION}.zip \
    && tar -xf opengfx-${GFX_VERSION}.tar \
    && mv opengfx-${GFX_VERSION}/* /app/baseset

RUN groupadd -r --gid 4101 openttd && \
  adduser --home /config --system --uid 4101 --gid 4101 openttd

RUN chmod u+x /app/openttd
RUN chown -R openttd:openttd /config /app

WORKDIR /app
VOLUME /config

USER openttd

EXPOSE 3977/tcp
EXPOSE 3979/tcp
EXPOSE 3979/udp

ENTRYPOINT [ "/app/openttd" ]

# #ENTRYPOINT ["/bin/bash"]